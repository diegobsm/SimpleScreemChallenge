package com.applaudostudios.singlescreenchallenge;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    TextView txvPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txvPhone = findViewById(R.id.txvPhone);
        txvPhone.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txvPhone:
                /**
                 * open action dial phone number
                 */
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:+503 "+txvPhone.getText()));
                startActivity(i);
                break;
        }
    }
}
